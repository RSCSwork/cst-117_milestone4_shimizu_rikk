﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//This is my work, Rikk Shimizu


namespace MilestoneProject
{
    public partial class mainPage : Form
    {
        public mainPage()
        {
            InitializeComponent();
            PreloadData();
            dataGridView1.DataSource = inventoryList.displayItems();

        }//ends public home

        //build the object to peek into for inventory management
        InventoryManager inventoryList = new InventoryManager();

        private void AddBtn_Click(object sender, EventArgs e)
        {
            string nameOfItem;
            string description; 
            string manufacturer;
            int quantity;
            double price;

            //This if is checking to make sure that all text boxs have some form of text within them. 
            if (!String.IsNullOrEmpty(nameTxtBx.Text) && !String.IsNullOrEmpty(descriptionTxtBx.Text) 
                && !String.IsNullOrEmpty(quantityTxtBx.Text) && !String.IsNullOrEmpty(manufacturerTxtBx.Text) && !String.IsNullOrEmpty(pricePerItemTxtBx.Text))
            {
                nameOfItem = nameTxtBx.Text;
                description = descriptionTxtBx.Text;
                manufacturer = manufacturerTxtBx.Text;

                //If you made it to this if, it is making sure the numbers that are used are valid inputs
                if (int.TryParse(quantityTxtBx.Text, out quantity) && double.TryParse(pricePerItemTxtBx.Text, out price)) {
                    inventoryList.addNewItemToArray(nameOfItem, description, quantity, manufacturer, price);
                }//ends try parse if check
            }//ends text box not null check, later this could be expanded to white space characters. 
            addedWindow nWin = new addedWindow();
            nWin.Show();
            displayAllItems();
        }//ends add btn.

        //This method is for unit testing to see that information was stored to the object and can be retrieved while updating the page. 
        private void AddToTable(InventoryItem item)
        {
            int n = dataGridView1.Rows.Add();
            dataGridView1.Rows[n].Cells[0].Value = item.ItemName;
            dataGridView1.Rows[n].Cells[1].Value = item.ItemID;
            dataGridView1.Rows[n].Cells[2].Value = item.ItemDescription;
            dataGridView1.Rows[n].Cells[3].Value = item.ItemQuantity;
            dataGridView1.Rows[n].Cells[4].Value = item.ItemManufacturer;
            dataGridView1.Rows[n].Cells[5].Value = item.PricePerItem;
        }//ends add to table

        /*
         * This method is used in conjunction with the displayItems 
         * found within the inventory Manager, since it is simpler to
         * post to the datagridview from the Form1 class, it makes 
         * more sense to send the needed info to post, to the form1
         * method. 
         */
        private void displayAllItems()
        {
            //Call a preload method for unit testing.
            //PreloadData();

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = inventoryList.displayItems();
            
        }//ends displayall

        //pre-plan ignore. was going to use this for unit testing but decided not to. 
        private void EditBtn_Click(object sender, EventArgs e)
        {
            
        }//ends edit button

        /*
         * will use search calls found in Inventory manager to then display information. 
         */
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            string nameOfItem;
            int IDnumber = -1; //set to -1 so it does not default to ID 0
            string description;
            string manufacturer;

            int inventoryListIndex = -1;

            if (!String.IsNullOrEmpty(nameTxtBx.Text) && int.TryParse(searchIDTxtBx.Text, out IDnumber))
            {
                nameOfItem = nameTxtBx.Text;
                inventoryListIndex = inventoryList.SearchItemNameAndID(nameOfItem, IDnumber);
            }else if (!String.IsNullOrEmpty(nameTxtBx.Text) && !String.IsNullOrEmpty(descriptionTxtBx.Text))
            {
                nameOfItem = nameTxtBx.Text;
                description = descriptionTxtBx.Text;
                inventoryListIndex = inventoryList.SearchItemNameAndDescription(nameOfItem, description);

            }
            else if (!String.IsNullOrEmpty(nameTxtBx.Text) && !String.IsNullOrEmpty(manufacturerTxtBx.Text))
            {
                nameOfItem = nameTxtBx.Text;
                manufacturer = manufacturerTxtBx.Text;
                inventoryListIndex = inventoryList.searchItemNameAndManufacturer(nameOfItem, manufacturer);
            }//ends if else tree

            if (inventoryListIndex == -1)
            {
                MessageBox.Show("Your item was not found and could not be displayed!");
            }
            else
            {
                //InventoryItem[] itemList = inventoryList.displayItems();
                List<InventoryItem> itemList = inventoryList.displayItems();
                int count = 0, index = -1;
                foreach (InventoryItem i in itemList)
                {
                    if (i.ItemID == inventoryListIndex)
                    {
                        index = count;
                    }
                    count++;
                }
                nameTxtBx.Text = itemList[index].ItemName;
                searchIDTxtBx.Text = itemList[index].ItemID.ToString();
                descriptionTxtBx.Text = itemList[index].ItemDescription;
                quantityTxtBx.Text = itemList[index].ItemQuantity.ToString();
                manufacturerTxtBx.Text = itemList[index].ItemManufacturer;
                pricePerItemTxtBx.Text = itemList[index].PricePerItem.ToString();



                /*
                nameTxtBx.Text = itemList[inventoryListIndex].ItemName;
                searchIDTxtBx.Text = itemList[inventoryListIndex].ItemID.ToString();
                descriptionTxtBx.Text = itemList[inventoryListIndex].ItemDescription;
                quantityTxtBx.Text = itemList[inventoryListIndex].ItemQuantity.ToString();
                manufacturerTxtBx.Text = itemList[inventoryListIndex].ItemManufacturer;
                pricePerItemTxtBx.Text = itemList[inventoryListIndex].PricePerItem.ToString();
                */
            }//ends if else for item found and displays the item. 
        }//ends search button

        private void RemoveBtn_Click(object sender, EventArgs e)
        {
            int IDnumber;
            if(int.TryParse(searchIDTxtBx.Text, out IDnumber))
            {
                inventoryList.removeItemFromArray(IDnumber);
            }//ends try parse if and calls remove.
            displayAllItems();
        }//ends remove button

        /*
         * calls the re stock method using the name and the amount to add. 
         */
        private void RestockBtn_Click(object sender, EventArgs e)
        {
            string name;
            int quantityToAdd = 0;

            if (!String.IsNullOrEmpty(nameTxtBx.Text) && int.TryParse(quantityTxtBx.Text, out quantityToAdd)) { 
                name = nameTxtBx.Text;
                inventoryList.restockItems(name, quantityToAdd);
            }//ends if
            displayAllItems();
        }//ends re stock button

        private void InventoryDisplayBtn_Click(object sender, EventArgs e)
        {
            displayAllItems();
        }

        private void ClrTxtBxsBtn_Click(object sender, EventArgs e)
        {
            nameTxtBx.Text = "";
            descriptionTxtBx.Text = "";
            quantityTxtBx.Text = "";
            manufacturerTxtBx.Text = "";
            pricePerItemTxtBx.Text = "";
            searchIDTxtBx.Text = "";

        }

        public void PreloadData()
        {
            inventoryList.addNewItemToArray("Blazer", "vane", 100, "bohning", 0.50);
            inventoryList.addNewItemToArray("aae", "vane", 100, "generic", 0.20);
            inventoryList.addNewItemToArray("heat", "vane", 100, "bohning", 0.40);
            inventoryList.addNewItemToArray("feather", "vane", 100, "generic", 0.10);
        }//ends preload




    }//ends class Home
}//ends namespace
