﻿namespace MilestoneProject
{
    partial class mainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addBtn = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.nameTxtBx = new System.Windows.Forms.TextBox();
            this.descriptionTxtBx = new System.Windows.Forms.TextBox();
            this.quantityTxtBx = new System.Windows.Forms.TextBox();
            this.manufacturerTxtBx = new System.Windows.Forms.TextBox();
            this.nameLbl = new System.Windows.Forms.Label();
            this.descriptionLbl = new System.Windows.Forms.Label();
            this.quantityLbl = new System.Windows.Forms.Label();
            this.manufacturerLbl = new System.Windows.Forms.Label();
            this.removeBtn = new System.Windows.Forms.Button();
            this.moveToBtn = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.moveToLbl = new System.Windows.Forms.Label();
            this.pricePerItemLbl = new System.Windows.Forms.Label();
            this.pricePerItemTxtBx = new System.Windows.Forms.TextBox();
            this.editBtn = new System.Windows.Forms.Button();
            this.searchIDLbl = new System.Windows.Forms.Label();
            this.searchIDTxtBx = new System.Windows.Forms.TextBox();
            this.restockBtn = new System.Windows.Forms.Button();
            this.inventoryDisplayBtn = new System.Windows.Forms.Button();
            this.clrTxtBxsBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(9, 220);
            this.addBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.addBtn.MinimumSize = new System.Drawing.Size(67, 32);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(67, 33);
            this.addBtn.TabIndex = 0;
            this.addBtn.Text = "Add";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(80, 220);
            this.searchBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.searchBtn.MinimumSize = new System.Drawing.Size(67, 32);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(67, 32);
            this.searchBtn.TabIndex = 1;
            this.searchBtn.Text = "Search";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // nameTxtBx
            // 
            this.nameTxtBx.Location = new System.Drawing.Point(107, 29);
            this.nameTxtBx.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.nameTxtBx.Name = "nameTxtBx";
            this.nameTxtBx.Size = new System.Drawing.Size(68, 20);
            this.nameTxtBx.TabIndex = 2;
            // 
            // descriptionTxtBx
            // 
            this.descriptionTxtBx.Location = new System.Drawing.Point(107, 62);
            this.descriptionTxtBx.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.descriptionTxtBx.Name = "descriptionTxtBx";
            this.descriptionTxtBx.Size = new System.Drawing.Size(68, 20);
            this.descriptionTxtBx.TabIndex = 4;
            // 
            // quantityTxtBx
            // 
            this.quantityTxtBx.Location = new System.Drawing.Point(107, 94);
            this.quantityTxtBx.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.quantityTxtBx.Name = "quantityTxtBx";
            this.quantityTxtBx.Size = new System.Drawing.Size(68, 20);
            this.quantityTxtBx.TabIndex = 5;
            // 
            // manufacturerTxtBx
            // 
            this.manufacturerTxtBx.Location = new System.Drawing.Point(107, 125);
            this.manufacturerTxtBx.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.manufacturerTxtBx.Name = "manufacturerTxtBx";
            this.manufacturerTxtBx.Size = new System.Drawing.Size(68, 20);
            this.manufacturerTxtBx.TabIndex = 6;
            // 
            // nameLbl
            // 
            this.nameLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameLbl.AutoSize = true;
            this.nameLbl.Location = new System.Drawing.Point(8, 29);
            this.nameLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nameLbl.MinimumSize = new System.Drawing.Size(67, 16);
            this.nameLbl.Name = "nameLbl";
            this.nameLbl.Size = new System.Drawing.Size(67, 16);
            this.nameLbl.TabIndex = 7;
            this.nameLbl.Text = "Name";
            // 
            // descriptionLbl
            // 
            this.descriptionLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.descriptionLbl.AutoSize = true;
            this.descriptionLbl.Location = new System.Drawing.Point(8, 62);
            this.descriptionLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.descriptionLbl.MinimumSize = new System.Drawing.Size(67, 16);
            this.descriptionLbl.Name = "descriptionLbl";
            this.descriptionLbl.Size = new System.Drawing.Size(67, 16);
            this.descriptionLbl.TabIndex = 9;
            this.descriptionLbl.Text = "Description";
            // 
            // quantityLbl
            // 
            this.quantityLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.quantityLbl.AutoSize = true;
            this.quantityLbl.Location = new System.Drawing.Point(8, 94);
            this.quantityLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.quantityLbl.MinimumSize = new System.Drawing.Size(67, 16);
            this.quantityLbl.Name = "quantityLbl";
            this.quantityLbl.Size = new System.Drawing.Size(67, 16);
            this.quantityLbl.TabIndex = 10;
            this.quantityLbl.Text = "Quantity";
            // 
            // manufacturerLbl
            // 
            this.manufacturerLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.manufacturerLbl.AutoSize = true;
            this.manufacturerLbl.Location = new System.Drawing.Point(8, 125);
            this.manufacturerLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.manufacturerLbl.MinimumSize = new System.Drawing.Size(67, 16);
            this.manufacturerLbl.Name = "manufacturerLbl";
            this.manufacturerLbl.Size = new System.Drawing.Size(70, 16);
            this.manufacturerLbl.TabIndex = 11;
            this.manufacturerLbl.Text = "Manufacturer";
            // 
            // removeBtn
            // 
            this.removeBtn.Location = new System.Drawing.Point(9, 268);
            this.removeBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.removeBtn.MinimumSize = new System.Drawing.Size(67, 32);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(67, 32);
            this.removeBtn.TabIndex = 12;
            this.removeBtn.Text = "Remove";
            this.removeBtn.UseVisualStyleBackColor = true;
            this.removeBtn.Click += new System.EventHandler(this.RemoveBtn_Click);
            // 
            // moveToBtn
            // 
            this.moveToBtn.Location = new System.Drawing.Point(57, 376);
            this.moveToBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.moveToBtn.MinimumSize = new System.Drawing.Size(67, 16);
            this.moveToBtn.Name = "moveToBtn";
            this.moveToBtn.Size = new System.Drawing.Size(67, 17);
            this.moveToBtn.TabIndex = 13;
            this.moveToBtn.Text = "Move To";
            this.moveToBtn.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(204, 29);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(711, 396);
            this.dataGridView1.TabIndex = 14;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(128, 376);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(47, 20);
            this.textBox1.TabIndex = 15;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(11, 376);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(47, 20);
            this.textBox2.TabIndex = 16;
            // 
            // moveToLbl
            // 
            this.moveToLbl.AutoSize = true;
            this.moveToLbl.Location = new System.Drawing.Point(8, 361);
            this.moveToLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.moveToLbl.MinimumSize = new System.Drawing.Size(167, 0);
            this.moveToLbl.Name = "moveToLbl";
            this.moveToLbl.Size = new System.Drawing.Size(167, 13);
            this.moveToLbl.TabIndex = 17;
            this.moveToLbl.Text = "Move from one ID to another";
            this.moveToLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pricePerItemLbl
            // 
            this.pricePerItemLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pricePerItemLbl.AutoSize = true;
            this.pricePerItemLbl.Location = new System.Drawing.Point(8, 152);
            this.pricePerItemLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.pricePerItemLbl.MinimumSize = new System.Drawing.Size(67, 16);
            this.pricePerItemLbl.Name = "pricePerItemLbl";
            this.pricePerItemLbl.Size = new System.Drawing.Size(72, 16);
            this.pricePerItemLbl.TabIndex = 18;
            this.pricePerItemLbl.Text = "Price per Item";
            // 
            // pricePerItemTxtBx
            // 
            this.pricePerItemTxtBx.Location = new System.Drawing.Point(107, 152);
            this.pricePerItemTxtBx.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pricePerItemTxtBx.Name = "pricePerItemTxtBx";
            this.pricePerItemTxtBx.Size = new System.Drawing.Size(68, 20);
            this.pricePerItemTxtBx.TabIndex = 19;
            // 
            // editBtn
            // 
            this.editBtn.Location = new System.Drawing.Point(80, 268);
            this.editBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.editBtn.MinimumSize = new System.Drawing.Size(67, 32);
            this.editBtn.Name = "editBtn";
            this.editBtn.Size = new System.Drawing.Size(67, 32);
            this.editBtn.TabIndex = 20;
            this.editBtn.Text = "Edit";
            this.editBtn.UseVisualStyleBackColor = true;
            this.editBtn.Click += new System.EventHandler(this.EditBtn_Click);
            // 
            // searchIDLbl
            // 
            this.searchIDLbl.AutoSize = true;
            this.searchIDLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchIDLbl.Location = new System.Drawing.Point(3, 181);
            this.searchIDLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.searchIDLbl.Name = "searchIDLbl";
            this.searchIDLbl.Size = new System.Drawing.Size(95, 9);
            this.searchIDLbl.TabIndex = 21;
            this.searchIDLbl.Text = "ID  for search and remove:";
            // 
            // searchIDTxtBx
            // 
            this.searchIDTxtBx.Location = new System.Drawing.Point(107, 177);
            this.searchIDTxtBx.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.searchIDTxtBx.Name = "searchIDTxtBx";
            this.searchIDTxtBx.Size = new System.Drawing.Size(68, 20);
            this.searchIDTxtBx.TabIndex = 22;
            // 
            // restockBtn
            // 
            this.restockBtn.Location = new System.Drawing.Point(9, 316);
            this.restockBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.restockBtn.Name = "restockBtn";
            this.restockBtn.Size = new System.Drawing.Size(67, 31);
            this.restockBtn.TabIndex = 23;
            this.restockBtn.Text = "Re-Stock";
            this.restockBtn.UseVisualStyleBackColor = true;
            this.restockBtn.Click += new System.EventHandler(this.RestockBtn_Click);
            // 
            // inventoryDisplayBtn
            // 
            this.inventoryDisplayBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inventoryDisplayBtn.Location = new System.Drawing.Point(80, 316);
            this.inventoryDisplayBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.inventoryDisplayBtn.Name = "inventoryDisplayBtn";
            this.inventoryDisplayBtn.Size = new System.Drawing.Size(67, 31);
            this.inventoryDisplayBtn.TabIndex = 24;
            this.inventoryDisplayBtn.Text = "Display Inventory";
            this.inventoryDisplayBtn.UseVisualStyleBackColor = true;
            this.inventoryDisplayBtn.Click += new System.EventHandler(this.InventoryDisplayBtn_Click);
            // 
            // clrTxtBxsBtn
            // 
            this.clrTxtBxsBtn.Location = new System.Drawing.Point(11, 410);
            this.clrTxtBxsBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.clrTxtBxsBtn.Name = "clrTxtBxsBtn";
            this.clrTxtBxsBtn.Size = new System.Drawing.Size(163, 24);
            this.clrTxtBxsBtn.TabIndex = 25;
            this.clrTxtBxsBtn.Text = "clear text boxes";
            this.clrTxtBxsBtn.UseVisualStyleBackColor = true;
            this.clrTxtBxsBtn.Click += new System.EventHandler(this.ClrTxtBxsBtn_Click);
            // 
            // mainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 477);
            this.Controls.Add(this.clrTxtBxsBtn);
            this.Controls.Add(this.inventoryDisplayBtn);
            this.Controls.Add(this.restockBtn);
            this.Controls.Add(this.searchIDTxtBx);
            this.Controls.Add(this.searchIDLbl);
            this.Controls.Add(this.editBtn);
            this.Controls.Add(this.pricePerItemTxtBx);
            this.Controls.Add(this.pricePerItemLbl);
            this.Controls.Add(this.moveToLbl);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.moveToBtn);
            this.Controls.Add(this.removeBtn);
            this.Controls.Add(this.manufacturerLbl);
            this.Controls.Add(this.quantityLbl);
            this.Controls.Add(this.descriptionLbl);
            this.Controls.Add(this.nameLbl);
            this.Controls.Add(this.manufacturerTxtBx);
            this.Controls.Add(this.quantityTxtBx);
            this.Controls.Add(this.descriptionTxtBx);
            this.Controls.Add(this.nameTxtBx);
            this.Controls.Add(this.searchBtn);
            this.Controls.Add(this.addBtn);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "mainPage";
            this.Text = "Inventory Management System";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.TextBox nameTxtBx;
        private System.Windows.Forms.TextBox descriptionTxtBx;
        private System.Windows.Forms.TextBox quantityTxtBx;
        private System.Windows.Forms.TextBox manufacturerTxtBx;
        private System.Windows.Forms.Label nameLbl;
        private System.Windows.Forms.Label descriptionLbl;
        private System.Windows.Forms.Label quantityLbl;
        private System.Windows.Forms.Label manufacturerLbl;
        private System.Windows.Forms.Button removeBtn;
        private System.Windows.Forms.Button moveToBtn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label moveToLbl;
        private System.Windows.Forms.Label pricePerItemLbl;
        private System.Windows.Forms.TextBox pricePerItemTxtBx;
        private System.Windows.Forms.Button editBtn;
        private System.Windows.Forms.Label searchIDLbl;
        private System.Windows.Forms.TextBox searchIDTxtBx;
        private System.Windows.Forms.Button restockBtn;
        private System.Windows.Forms.Button inventoryDisplayBtn;
        private System.Windows.Forms.Button clrTxtBxsBtn;
    }
}

