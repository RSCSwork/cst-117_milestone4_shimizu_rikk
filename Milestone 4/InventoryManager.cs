﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//This is my work, Rikk Shimizu

namespace MilestoneProject
{
    class InventoryManager
    {
        /*
         * an array is required for managing the inventory
         * this has to be of the type InventoryItem since 
         * that is what we are storing. I have set the number 
         * to 400 for now to allow for a large number of items 
         * to be added. 
         * In a normal situation I would choose a List instead
         * of an array.
         */
        //InventoryItem[] shopInventory = new InventoryItem[400];
        List<InventoryItem> shopInventory = new List<InventoryItem>();
        /*
         * we also need a counter that should also match the 
         * ID numbers that are set in the InventoryItem class.
         */
        int inventoryIDandCounter = 0;

        /*
         * This is similar to the constructor in InventoryItem
         * Basically just add items into the array slot with
         * their appropriate information.
         * 
         * @ param string itemName, string itemDescription, int itemQuantity, string itemManufacturer
         * @ param double pricePerItem
         */
        public void addNewItemToArray(string itemName, string itemDescription, int itemQuantity, string itemManufacturer, double pricePerItem)
        {
            //shopInventory[inventoryIDandCounter] = new InventoryItem(itemName, this.inventoryIDandCounter, itemDescription, itemQuantity, itemManufacturer, pricePerItem);
            shopInventory.Add(new InventoryItem(itemName, this.inventoryIDandCounter, itemDescription, itemQuantity, itemManufacturer, pricePerItem));
            inventoryIDandCounter++;
        }//ends add new item


        /*
         * This method is not a true delete of the index point
         * It simply replaces the current information with the next slots info.
         * 
         * Current implementation means that ID numbers are not updated... I personally
         * do not see a problem with this, if you knew an item by a certain ID it should
         * stay the same. 
         * @param int indexToRemove
         */
        public void removeItemFromArray(int indexToRemove)
        {
            //simply shift all values over to the 
            shopInventory.RemoveAt(indexToRemove);
            /*
            for (int index = indexToRemove; index < shopInventory.Length; index++)
            {
                if (shopInventory[index + 1] == null)
                {
                    shopInventory[index] = null;
                    return;
                }
                else 
                    shopInventory[index] = shopInventory[index + 1];
            }
            */
            
        }//ends remove item

        /*
         * Calls method for the shop item to add quantity
         * to a given index
         * 
         * @param string name, int quantityToAdd
         */
        public void restockItems(string name, int quantityToAdd)
        {
            int count = 0, index = -1;
            foreach(InventoryItem i in shopInventory)
            {
                if (i.ItemName == name)
                    index = count;
                count++;
            }
            if (index == -1)
                return;
            else
                shopInventory[index].addQuantity(quantityToAdd);
            return;
            

            /*
            for (int i = 0; i < shopInventory.Length; i++)
            {
                if(shopInventory[i].ItemName == name)
                {
                    shopInventory[i].addQuantity(quantityToAdd);
                    return;
                }//ends if
            }//ends for
            */

        }//ends re-stock

        /*
         * Since it is more difficult to access the dataGrid view from a different class, send the 
         * array out from this method, this array can be used in the Form1 class to actually
         * display the items. 
         */
        //public InventoryItem[] displayItems()
        public List<InventoryItem> displayItems()
        {
            return shopInventory;
        }//ends display

        /*
         * Design notes:
         * The following three search methods take in two criteria
         * the name is always required, these three combinations 
         * were chosen because they made the most sense in terms
         * of valuable information to search on. quantity and price
         * per item could apply to many different items. 
         * All three methods are essentially identical with the 
         * exception of the second search criteria
         */

        /*
         * Takes in the name of the item, goes through the array to 
         * find the matching name and secondary search criteria
         * returns the index of the item. If the items index is not
         * found then a -1 is returned and an error message displayed.
         * 
         * @param string name, int ID
         * @return int 
         */ 
        public int SearchItemNameAndID(string name, int ID) 
        {
            int count = 0, index = -1;
            foreach (InventoryItem i in shopInventory)
            {
                if (i.ItemName == name && i.ItemID == ID)
                {
                    index = count;
                    return index;
                }
                count++;
            }
            return index;

            /*
            for (int i = 0; i < shopInventory.Length; i++)
            {
                if(shopInventory[i].ItemName == name && shopInventory[i].ItemID == ID)
                {
                    return i;
                }//ends if
            }//ends for

            return -1; //if a -1 is returned then the name was not found.
            */

        }//ends search

        /*
         * @param string name, string description 
         * @return int 
         */
        public int SearchItemNameAndDescription(string name, string description)
        {
            int count = 0, index = -1;
            foreach (InventoryItem i in shopInventory)
            {
                if (i.ItemName == name && i.ItemDescription == description)
                {
                    index = count;
                    return index;
                }
                count++;
            }
            return index;
            /*
            for (int i = 0; i < shopInventory.Length; i++)
            {
                if (shopInventory[i].ItemName == name && shopInventory[i].ItemDescription == description)
                {
                    return i;
                }//ends if
            }//ends for

            return -1; //if a -1 is returned then the name was not found.
            */

        }//ends search

        /* 
         * @param string name, string manufacturer
         * @return int 
         */
        public int searchItemNameAndManufacturer(string name, string manufacturer)
        {
            int count = 0, index = -1;
            foreach (InventoryItem i in shopInventory)
            {
                if (i.ItemName == name && i.ItemManufacturer == manufacturer)
                {
                    index = count;
                    return index;
                }
                count++;
            }
            return index;

            /*
            for (int i = 0; i < shopInventory.Length; i++)
            {
                if (shopInventory[i].ItemName == name && shopInventory[i].ItemManufacturer == manufacturer)
                {
                    return i;
                }//ends if
            }//ends for

            return -1; //if a -1 is returned then the name was not found.
            */
        }//ends search

    }//ends class
}//ends namespace
